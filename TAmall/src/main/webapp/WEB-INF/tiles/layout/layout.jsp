<%@ page language="java" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<%@ taglib prefix="tiles" uri="http://tiles.apache.org/tags-tiles"%>
<c:set var="tt" value="<%=System.currentTimeMillis() %>"/>

<!DOCTYPE html>
<html>
	<head>
		<meta charset="utf-8">
		<title>Mall</title>
		
		<!-- Basic Styles -->
		<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/css/bootstrap.min.css"/>">
		<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/webjars/font-awesome/4.7.0/css/font-awesome.min.css"/>">
		
		<!-- CSS -->
    	<link rel="stylesheet" type="text/css" media="screen" href="<c:url value="/css/style.css"/>">
		
		<%-- <script src="<c:url value="/webjars/jquery/3.2.1/dist/jquery.min.js?v=${tt}"/>"></script> --%>
		<!-- jquery는 webjars 방식을 사용하였습니다. -->
		<script src="<c:url value="/webjars/jquery/3.2.1/dist/jquery.min.js?v=${tt}"/>"></script>
		
		<!-- BOOTSTRAP JS -->
		<script src="<c:url value="/js/bootstrap/bootstrap.min.js"/>"></script>
		
		<!-- common JS -->
		<script src="<c:url value="/js/common/commonUtil.js?v=${tt}"/>"></script>
		
		
	</head>
	<body>
	     <tiles:insertAttribute name="header" />
	
	     <tiles:insertAttribute name="content" />
	      
	     <tiles:insertAttribute name="footer" />
	</body>
</html>