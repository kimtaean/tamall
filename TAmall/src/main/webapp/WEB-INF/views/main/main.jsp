<%@ page language="java" contentType="text/html; charset=UTF-8"%> 
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fn" uri="http://java.sun.com/jsp/jstl/functions"%> 
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>
<script src="<c:url value="/webjars/jquery-form/4.2.1/jquery.form.min.js?v=${tt}"/>"></script>
<script src="<c:url value="/js/main/main.js?v=${tt}"/>"></script>
<!-- 
이 곳은 main.jsp 페이지 입니다. 레이아웃은 tiles를 연동하여 header와 footer가 포함되어 있습니다.
또한 기존 jsp 페이지에서 사용할 jstl 등이 포함되어 있습니다.
-->
<!-- start header -->
<header>
	<div class="con_2">
		<ul class="topMenu" style="">
			<li>
				<a id="aaa" onclick="aa();">
					<img src="image/icon/menu.png" alt="menu" class="pull-left pointer topM"/>
				</a>
			</li>
			<li>
				<a href="/">
					<img src="image/logo.png" alt="logo" class="pointer" style="margin-top: -20px;"/>
				</a>
			</li>
			<li>
				<a><img src="image/icon/shopping-cart.png" alt="shoppingcart" class="pull-right pointer topM"/></a>
				<a><img src="image/icon/search.png" alt="search" class="pull-right pointer topM"/></a>
			</li>
		</ul>
	
		<ul id="subMenu" class="subMenuS">
			<li><a href="#">menu1</a></li>
			<li><a href="#">menu2</a></li>
			<li><a href="#">menu3</a></li>
			<li><a href="#">menu4</a></li>
		</ul>
	</div>
	
	<div class="con_2 searchBox">
		<input type="text" placeholder="search" class="width_100 form-control"/>
   	</div>

</header>
<!-- end header -->

<!-- start slider -->
<section>
	<div>
		<div>
			<img src="image/main.png" alt="" class="imageM">
		</div>
	</div>
</section>
<!-- end slider -->

<!-- start content -->
<section style="margin-bottom: 5px;">
	<div class="container">
		<div>
      		<h2>Category</h2>
	  		<hr class="hrS">
		</div>
		<div>
			<ul class="cateMenu">
				<li>
					<img src="image/cate1.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate2.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate3.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate4.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate5.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate6.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate7.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
				<li>
					<img src="image/cate8.png" alt="" class="imageM">
					<p class="mItemT">Item</p>
				</li>
			</ul>
		</div>
    </div>
</section>
<!-- end content -->

<!-- start content -->
<section>
	<div>
		<img src="image/banner.png" alt="" class="imageM">
    </div>
</section>
<!-- end content -->

<!-- start content -->
<section>
	<c:forEach var="list" items="${resultList}">
		<div class="container itemList">
		<input type="hidden" id="idx" name="idx" value="${list.idx }"/>
			<hr class="hrS">
					<div>
						<div><h6>${list.item_name }</h6></div>
						<div class="proImg">
							<img src="image/product1.png" alt="상품" class="imageM">
						</div>
						<div class="proTextBox">
							<div class="proText">
									<p>설명 : ${list.descr }</p>
									<p>요약 : ${list.summary }</p>
									<p>타입 : ${list.skin_type_code}</p>
								<!-- <p class="textGray" style="margin-bottom: 0;">
									hanskin<br/>
									Bringtening<br/>
									sale 30%<br/>
								</p> -->
							</div>
							<div>
								<p class="textRed proPrice">$${list.price }</p>
								<img src="image/plusBtn.png" alt="button" width="25" height="25" name="${list.idx }" class="pull-right pointer shBasket">
							</div>
						</div>
					</div>
		  <hr class="hrS">
	    </div>
   </c:forEach>
    
</section>
<!-- end content -->

	<!-- 장바구니 Modal -->
	<div class="modal fade" id="modal_itemList" tabindex="-1" role="dialog" aria-labelledby="modal_cmmnCode_detail_Label" aria-hidden="true">
		<div class="modal-dialog">
			<div class="modal-content">
				<div class="modal-header">
					<!-- <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button> -->
					<h4 class="modal-title" id="modal_cmmnCode_detail_Label">장바구니 추가</h4>
				</div>
				<div class="modal-body">
					<div class="row">
						<div class="col-md-12">
							<div class="table-responsive">
								<div id="itemModalList"></div>
							</div>
						</div>
					</div>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn btn-primary" >추가</button>
					<button type="button" class="btn btn-default" data-dismiss="modal">닫기</button>
				</div>
			</div>
			<!-- /.modal-content -->
		</div>
		<!-- /.modal-dialog -->
	</div>
	<!-- /.Modal 상세코드 -->


<footer>
	<div class="container">
		<span class="spanF">TAmall</span>
	</div>
</footer>