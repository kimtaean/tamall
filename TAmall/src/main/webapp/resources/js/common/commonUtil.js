$(document).ready(function(){
	
});




/*
 * AJAX 
 */
function fnCallAjax(callType, url, returnDataType, paramData, callBackFn, boolShowLayer){
	if(boolShowLayer){
		$(".overlay").show();
	}
	
	$.ajax({
		url : url,
		type : callType,
		dataType: returnDataType,
		data : paramData,
		success: function(data){
			//console.log(">> in success");
		},      
		error: function(xhr,status,error){
			//console.log(">> in error");
		},
		complete: function(data){
			//console.log(">> in complete");
			//console.log(data);
			callBackFn(data);
			
			if(boolShowLayer){
				$(".overlay").hide();
			}
		} 
	});
}

//nonSubmit 클래스인 button태그가 submit 안되게 하는 기능
function setBtn_noSubmit(){
	$('.nonSubmit').on('click',function(event){
		event.preventDefault();
	});
}

