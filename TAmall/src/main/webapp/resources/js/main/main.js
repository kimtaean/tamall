$(document).ready(function(){
	$("#aaa").click(function(){
		$("#subMenu").slideToggle();
	});
	
	
	//상품 아이템 클릭이벤트 설정
	$(".shBasket").on("click",function(e){
		/*
		 *상품 정보 호출(장바구니)
		 *상품의 idx 값을 param으로 전송하여 이를 이용한 데이터를 호출
		 */
	    var param = {"idx":e.currentTarget.name};
	    console.log(e);
		fnCallAjax("POST", "/main/procItemManage.ajax", "json", param, procItemManage_callback, true);
	});
	
	
});

/*
 *장바구니 modal 설정 callback함수
 *json을 이용한 장바구니 modal 데이터 호출 
*/
function procItemManage_callback(data) {
	var json = data.responseJSON;
	
	if(json.rstFlag){
		var htmlText = '';
		var idx = json.idx; //상세보기 이동시 필요한 idx
		
		$resultBody = $("#itemModalList");
		$.each(json.resultList, function(i,v){
		
			htmlText += '<div>';
			htmlText += 	'<input type="hidden" id="idx" name="idx" value="'+idx+'">';
			htmlText += 	'<div>';
			htmlText += 	'<div><h6>'+v.item_name+'</h6></div>'
			htmlText +=		'<div class="proImg">';
			htmlText +=			'<img src="image/product1.png" alt="상품" class="imageM">';
			htmlText +=		'</div>';
			htmlText +=		'<div class="proTextBox">'; 
			htmlText +=			'<div class="proText">';
			htmlText +=				'<p>설명 : '+v.descr+'</p>';
			htmlText +=				'<p>요약 : '+v.summary+'</p>';
			htmlText +=				'<p>타입 : '+v.skin_type_code+'</p>';
			htmlText +=			'</div>';
			htmlText +=		'</div>';
			htmlText +=		'<div>';
			htmlText +=			'<p class="textRed proPrice">$'+v.price+'</p>';
			htmlText +=		'</div>';
			htmlText +=		'</div>'
			htmlText += 	'</div>';
			htmlText += '</div>';
			
		});
		
		$resultBody.empty();
		$resultBody.append(htmlText);
		$("#modal_itemList").modal('show');
	}else{
		alert('상품 확인 중 오류가 발생하였습니다.\r페이지 새로고침(F5) 후 다시 시도해주세요.');
	}
	
}