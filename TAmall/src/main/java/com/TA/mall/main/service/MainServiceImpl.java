package com.TA.mall.main.service;


import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.TA.mall.main.vo.MainVO;


@Service("mainService")
public class MainServiceImpl implements MainService{

	private static final Logger logger = LoggerFactory.getLogger(MainServiceImpl.class);
	
	@Autowired
	private MainMapper mainMapper;
	
	//메인 상품 리스트
	@Override
	public List<MainVO> selectItemList(MainVO vo) throws Exception {
		return mainMapper.selectItemList(vo);
	}
	
	//메인 상품 모달
	@Override
	public List<MainVO> selectItemModalList(MainVO vo) throws Exception {
		return mainMapper.selectItemModalList(vo);
	}
	
}