package com.TA.mall.main.service;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;

import com.TA.mall.main.vo.MainVO;

@Mapper
public interface MainMapper {

	//메인 상품 리스트
	public List<MainVO> selectItemList(MainVO vo);

	//메인 상품 모달
	public List<MainVO> selectItemModalList(MainVO vo);

}
