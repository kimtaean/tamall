package com.TA.mall.main.vo;

import com.TA.mall.common.vo.DefaultVO;

/**
 * 상품 데이터를 담아 낼 VO
 * 
 * */

public class MainVO extends DefaultVO {

	private int idx; //번호
	
	private String item_code; //상품 코드
	
	private String brand_code; //제조사 코드
	
	private String item_name; //상품명
	
	private String descr; //설명
	
	private String summary; //요약설명
	
	private int price; //가격
	
	private int item_img; //상품 이미지
	
	private int desc_img; //설명 이미지
	
	private int vol_total; //전체 용량
	
	private int vol; //개별 용량
	
	private String vol_unit; //용량 단위
	
	private int EA; //개수
	
	private String cat_code; //제품분류 코드
	
	private String cat_dtl_code; //기능별 상세분류 코드
	
	private String skin_type_code; //피부타입 코드
	
	public int getIdx() {
		return idx;
	}

	public void setIdx(int idx) {
		this.idx = idx;
	}

	public String getItem_code() {
		return item_code;
	}

	public void setItem_code(String item_code) {
		this.item_code = item_code;
	}

	public String getBrand_code() {
		return brand_code;
	}

	public void setBrand_code(String brand_code) {
		this.brand_code = brand_code;
	}

	public String getItem_name() {
		return item_name;
	}

	public void setItem_name(String item_name) {
		this.item_name = item_name;
	}

	public String getDescr() {
		return descr;
	}

	public void setDescr(String descr) {
		this.descr = descr;
	}

	public String getSummary() {
		return summary;
	}

	public void setSummary(String summary) {
		this.summary = summary;
	}

	public int getPrice() {
		return price;
	}

	public void setPrice(int price) {
		this.price = price;
	}

	public int getItem_img() {
		return item_img;
	}

	public void setItem_img(int item_img) {
		this.item_img = item_img;
	}

	public int getDesc_img() {
		return desc_img;
	}

	public void setDesc_img(int desc_img) {
		this.desc_img = desc_img;
	}

	public int getVol_total() {
		return vol_total;
	}

	public void setVol_total(int vol_total) {
		this.vol_total = vol_total;
	}

	public int getVol() {
		return vol;
	}

	public void setVol(int vol) {
		this.vol = vol;
	}

	public String getVol_unit() {
		return vol_unit;
	}

	public void setVol_unit(String vol_unit) {
		this.vol_unit = vol_unit;
	}

	public int getEA() {
		return EA;
	}

	public void setEA(int eA) {
		EA = eA;
	}

	public String getCat_code() {
		return cat_code;
	}

	public void setCat_code(String cat_code) {
		this.cat_code = cat_code;
	}

	public String getCat_dtl_code() {
		return cat_dtl_code;
	}

	public void setCat_dtl_code(String cat_dtl_code) {
		this.cat_dtl_code = cat_dtl_code;
	}

	public String getSkin_type_code() {
		return skin_type_code;
	}

	public void setSkin_type_code(String skin_type_code) {
		this.skin_type_code = skin_type_code;
	}


	
	
	
}
