package com.TA.mall.main.service;

import java.util.List;

import com.TA.mall.main.vo.MainVO;

public interface MainService {

	//메인 상품 리스트
	public List<MainVO> selectItemList(MainVO vo) throws Exception;

	//메인 상품 모달
	public List<MainVO> selectItemModalList(MainVO vo) throws Exception;

}
