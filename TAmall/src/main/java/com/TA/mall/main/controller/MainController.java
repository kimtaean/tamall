package com.TA.mall.main.controller;

import java.util.HashMap;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import com.TA.mall.main.service.MainService;
import com.TA.mall.main.vo.MainVO;

/**
 * 메인 컨트롤러
 * MainController.java - MainService.java - MainServiceImpl.java - MainMapper.java - mainMapper_SQL.xml
 * 위의 흐름으로 DB데이터를 가져와 main.jsp에 tiles를 적용하여 데이터를 호출 시킨다. 
 */
@Controller
public class MainController {
	
	private static final Logger logger = LoggerFactory.getLogger(MainController.class);
	
	@Autowired
	private MainService mainService;
	
	@RequestMapping(value = "/")
	public String main_root(@ModelAttribute("searchVO") MainVO vo, Locale locale, Model model) throws Exception {
	
		List<MainVO> resultList = null;
		
		try {
			//메인 상품 리스트
			resultList = mainService.selectItemList(vo);
		
			model.addAttribute("resultList", resultList);
			
		} catch (Exception e) {
			
			logger.info(e.getMessage()); 
			e.printStackTrace(); 
			
		}
		
		
		return "/main/main.tiles";
	}
	
	/*@RequestMapping(value = "/main")
	public String main(Locale locale, Model model) {
		return "/main/main.tiles";
	}*/
	
	
	//장바구니 modal 생성 (ajax를 이용한 데이터 호출)
	@RequestMapping(value = "/main/procItemManage.ajax")
	@ResponseBody
	public Object procItemManage(MainVO vo,Locale locale, Model model) throws Exception{
		
		HashMap<String, Object> resultMap = new HashMap<String,Object>();
		boolean rstFlag = false;
		List<MainVO> resultList = null;
		int idx = vo.getIdx(); //idx를 이용하여 modal에 데이터 호출
		
		
		try {
			
			//메인 상품 리스트
			resultList = mainService.selectItemModalList(vo);
    		
    		//결과 리스트
			resultMap.put("resultList", resultList ); 
    		rstFlag = true;
    		
		} catch (Exception e) {
			logger.info(e.getMessage()); 
			e.printStackTrace();
			
		} finally {
			resultMap.put("idx", idx); //상품 리스트 상세보기를 위한 idx 값
			resultMap.put("rstFlag", rstFlag);
		}
		
		
		
		return resultMap;

	}
	
	
	
}
